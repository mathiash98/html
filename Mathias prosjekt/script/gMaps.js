function initMap() {
  var myLatLng = {lat: 60.385522, lng: 5.338409};

  var map = new google.maps.Map(document.getElementById('gMaps'), {
    zoom: 13,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Amalie Skram'
  });
}