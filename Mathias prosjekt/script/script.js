$(document).ready(function () {
    $(document).on('touchstart', function (e) {
        $(document).unbind('mousemove.hoverTest');
    });

    $('button[href^="#"]').click(function () {
        var target = $($(this).attr('href'));
        $('html, body').animate({
            scrollTop: target.offset().top
        }, 800);
    });

    $('#submit_button').click(function () {
        $('#message').css('background-color', '#49ec1c');
        $('#message').val('Thank you for contacting me! I will reach to you as soon as possible.');
    });
});

function setBulb(number) {
    var img = document.getElementById("light_bulb" + number);

    if (img.getAttribute('src') === 'images/OFF_lightbulb.png') {
        img.src = "images/ON_lightbulb.png";
        $("#window-" + number).show();
    } else if (img.getAttribute('src') === 'images/ON_lightbulb.png') {
        img.src = "images/OFF_lightbulb.png";
        $("#window-" + number).hide();
    }
}